package mobileweb.service;

import java.util.List;

import mobileweb.dto.ProductDTO;
import mobileweb.entity.ProductEntity;

public interface IProductService {
	ProductDTO save(ProductDTO productDTO);
	//ProductDTO update(ProductDTO productDTO);
//	void delete(long[] ids);
	List<ProductDTO> findAll();
	
	void delete(long id);
	
	ProductEntity getProductById(Long id);
}
